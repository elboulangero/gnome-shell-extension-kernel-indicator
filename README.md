No longer maintained!

This extension is now maintained by @pvizc under the uuid `kernel-indicator@pvizc.gitlab.com`.

Check it out at: https://gitlab.com/pvizc/gnome-shell-extension-kernel-indicator
